//! \verbatim
//! ##############################################################################
//! # Copyright (c) 2014 RTT AG
//! # All rights reserved.
//! # 
//! # RTT AG, Rosenheimer Str. 145, D-81671 Munich (Germany)
//! ##############################################################################
//! 
//! \author Timo Wiesemann <timo.wiesemann@rtt.ag>
//! \date 04-Dec-2014
//! additional docs : 
//! \endverbatim 
//!
//! //! \file  RegExTester.h
//! \brief Declaration of class RegExTester.

#include "RegExTester.h"
#include "NumberedEditPane.h"

#include <QStringBuilder>

const QString RegExTester::g_labelTemplate( QLatin1String("Results for line %1 / %2") );

RegExTester::RegExTester()
: m_currentResultPage(0)
{
	setupUi(this);

	pb_prev->hide();
	l_pageLabel->hide();
	pb_next->hide();

	connect( cb_exactMatch, SIGNAL( toggled(bool) ), SLOT(onExactMatchChanged(bool)) );
}

RegExTester::~RegExTester(){}

void RegExTester::closeEvent ( QCloseEvent * e ){
	clear();
}

void RegExTester::on_button_Close_clicked(){
	close();
}

void RegExTester::on_button_Clear_clicked(){
	txt_Result->clear();
	txt_SampleText->clear();
	txt_RegExp->clear();
}

void RegExTester::on_pb_prev_clicked()
{

	m_currentResultPage--;
	if( m_currentResultPage<=0 ) pb_prev->setDisabled(true);

	if( m_currentResultPage+1 < m_results.size() ) pb_next->setDisabled(false);

	displayResult();
}

void RegExTester::on_pb_next_clicked()
{

	m_currentResultPage++;
	if( m_currentResultPage+1>=m_results.size() ) pb_next->setDisabled(true);

	if( m_currentResultPage >0 ) pb_prev->setDisabled(false);

	displayResult();
}

void RegExTester::on_cb_lineWrap_toggled( bool checked )
{
	if( checked ){
		txt_SampleText->setLineWrapMode(QPlainTextEdit::WidgetWidth);
	} else {
		txt_SampleText->setLineWrapMode(QPlainTextEdit::NoWrap);
	}
}

void RegExTester::onExactMatchChanged( bool state )
{
	if( state ){
		rb_matchOnce->setChecked(true);
	}
}

void RegExTester::on_button_Test_clicked(){
	txt_Result->clear();
	m_results.clear();

	m_regExp.setPattern( txt_RegExp->text() );
	m_regExp.setMinimal( cb_setMinimal->isChecked() );

	if( cb_caseSensitive->isChecked() ){
		m_regExp.setCaseSensitivity(Qt::CaseSensitive);
	} else {
		m_regExp.setCaseSensitivity(Qt::CaseInsensitive);
	}

	// check for reg-ex validity - abort if invalid
	if( !m_regExp.isValid() ){
		m_currentResultPage = 0;
		m_results.push_back( QString("The regular expression pattern is invalid!\n\"%1\"").arg(m_regExp.errorString()) );
		displayResult();
		return;
	}

	// pre-process the haystack (i.e. split it up by lines - or not)
	QStringList haystackText;
	if( cb_matchSingleLine->isChecked() ){	// default
		haystackText.push_back( txt_SampleText->document()->toPlainText() );
	} else {
		// split it up by \n
		haystackText = txt_SampleText->document()->toPlainText().split('\n');
	}

	// try to match each line
	foreach( const QString& line, haystackText )
	{
		// match exactly - i.e. the whole expression; not partially
		if( cb_exactMatch->isChecked() ){
			bool matched = m_regExp.exactMatch( line );
			if( matched ){
				const QStringList capList( m_regExp.capturedTexts() );
				m_results.push_back( QString("MATCH! (%1 captures)").arg(QString::number(capList.size()-1)) );
				for(int i=1; i< capList.size(); ++i){
					m_results.back().append(QString("\n[%1] - \"%2\"").arg(QString::number(i)).arg(capList.at(i)));
				}
			} else {
				m_results.push_back( QString("NO MATCH!\nCould match the first %1 character(s).").arg(QString::number( m_regExp.matchedLength())) );
			}
		}
		else{
			// match partially - i.e. it it matches somewhere in the text

			if( rb_matchOnce->isChecked() ){
				if( m_regExp.indexIn(line) != -1 ){
					const QStringList capList( m_regExp.capturedTexts() );
					m_results.push_back( QString("MATCH! (%1 captures)").arg(QString::number(capList.size()-1)) );
					for(int i=1; i< capList.size(); ++i){
						m_results.back().append(QString("\n[%1] - \"%2\"").arg(QString::number(i)).arg(capList.at(i)));
					}
				} else {
					m_results.push_back( QLatin1String("NO MATCH!") );
				}
			} else {
				int pos = 0;
				int matchCount = 0;
				int capCount = 0;
				QString allTheCaptures;

				while ((pos = m_regExp.indexIn(line, pos)) != -1) {
					matchCount++;
					const QStringList capList( m_regExp.capturedTexts() );
					capCount = capList.size();
					for(int i=1; i< capList.size(); ++i){
						allTheCaptures.append(QString("\n[%1.%2] - \"%3\"")
							.arg(QString::number(matchCount))
							.arg(QString::number(i))
							.arg(capList.at(i)));
					}
					pos += m_regExp.matchedLength()<1 ? 1 : m_regExp.matchedLength();
				}

				if( matchCount>0 ){
					m_results.push_back( QString("MATCHED %1 times! (%2 captures) - [matchNo.captureNo]").arg(QString::number(matchCount)).arg(QString::number(capCount-1)) );
					m_results.back().append( QLatin1String("\n") + allTheCaptures );
				} else {
					m_results.push_back( QLatin1String("NO MATCH!") );
				}

			}
		}
	}

	m_currentResultPage = 0;
	pb_next->setDisabled(false);
	pb_prev->setDisabled(true);
	displayResult();
}

void RegExTester::clear(){
	txt_Result->clear();
}

void RegExTester::displayResult()
{
	l_pageLabel->setText( g_labelTemplate.arg(m_currentResultPage+1).arg(m_results.size()) );
	txt_Result->document()->setPlainText( m_results.at(m_currentResultPage) );
	if( m_results.size() > 1){
		pb_prev->show();
		pb_next->show();
		l_pageLabel->show();
	} else {
		pb_prev->hide();
		pb_next->hide();
		l_pageLabel->hide();
	}
}
