//! \verbatim
//! ##############################################################################
//! # Copyright (c) 2014 RTT AG
//! # All rights reserved.
//! # 
//! # RTT AG, Rosenheimer Str. 145, D-81671 Munich (Germany)
//! ##############################################################################
//! 
//! \author Timo Wiesemann <timo.wiesemann@rtt.ag>
//! \date 04-Dec-2014
//! additional docs : 
//! \endverbatim 
//! 
//! \file  RegExTester.h
//! \brief Declaration of class RegExTester.

#ifndef _REGEXTESTER_H_
#define _REGEXTESTER_H_

#include "ui_RegExTester.h"

class RegExTester: public QDialog, public Ui_TestRegularExpression
{
	Q_OBJECT
public:
 	RegExTester();
 	virtual ~RegExTester();

protected:
	 virtual void closeEvent ( QCloseEvent * e );

private slots:
	 void on_button_Close_clicked();
	 void on_button_Clear_clicked();
	 void on_button_Test_clicked();
	 void on_pb_prev_clicked();
	 void on_pb_next_clicked();
	 void on_cb_lineWrap_toggled( bool checked );
	 void onExactMatchChanged( bool state );

private:
	void clear();
	void displayResult();

private:
	QRegExp m_regExp;
	int m_currentResultPage;
	QStringList m_results;

	static const QString g_labelTemplate;
}; 

#endif  // _REGEXTESTER_H_
