//! \verbatim
//! ##############################################################################
//! # Copyright (c) 2014 RTT AG
//! # All rights reserved.
//! # 
//! # RTT AG, Rosenheimer Str. 145, D-81671 Munich (Germany)
//! ##############################################################################
//! 
//! \author Timo Wiesemann <timo.wiesemann@rtt.ag>
//! \date 04-Dec-2014
//! additional docs : 
//! \endverbatim 
//! 

#ifndef NUMBEREDEDITPANE_H_
#define NUMBEREDEDITPANE_H_

#include <QPlainTextEdit>
#include <QObject>

#include <QBoxLayout>
#include <QToolBar>
#include <QAction>

class QPaintEvent;
class QResizeEvent;
class QSize;
class QWidget;

class LineNumberArea;

class NumberedEditPane : public QPlainTextEdit
{
   Q_OBJECT

public:
   NumberedEditPane(QWidget *parent = 0);
   void lineNumberAreaPaintEvent(QPaintEvent *event);
   int lineNumberAreaWidth();
protected:
    void resizeEvent(QResizeEvent *event);

private slots:
    void updateLineNumberAreaWidth(int newBlockCount);
    void highlightCurrentLine();
    void updateLineNumberArea(const QRect &, int);
private:
    QWidget *lineNumberArea;
};

// =====================================================================

class LineNumberArea : public QWidget
{
public:
   LineNumberArea(NumberedEditPane *editor) : QWidget(editor) {
      m_editor = editor;
   }

   QSize sizeHint() const {
      return QSize(m_editor->lineNumberAreaWidth(), 0);
   }

protected:
   void paintEvent(QPaintEvent *event) {
      m_editor->lineNumberAreaPaintEvent(event);
   }

private:
   NumberedEditPane *m_editor;
};

#endif /* NUMBEREDEDITPANE_H_ */

