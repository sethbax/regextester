//! \verbatim
//! 
//! \author Timo Wiesemann <timo.wiesemann@googlemail.com>
//! \date 04-Dec-2014
//! additional docs : 
//! \endverbatim 
//! 

#include "RegExTester.h"

#include <QtWidgets/QApplication>


int main( int argc, char* argv[] )
{
	QApplication app( argc, argv );
	RegExTester dialog;
	dialog.show();

	const int res = app.exec();

	return res;
}